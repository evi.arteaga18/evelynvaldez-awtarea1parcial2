document.querySelector("#boton").addEventListener('click', mostrardatos);
function mostrardatos(){
    const xhttp = new XMLHttpRequest();
    xhttp.open('GET', 'estudiantes.json',true);
    xhttp.send();
    xhttp.onreadystatechange = function(){
        if(this.readyState == 4 && this.status == 200){
            let datos = JSON.parse(this.responseText);
            let res = document.querySelector('#res');
            res.innerHTML = '';
            for(let item of datos){
               res.innerHTML += ` 
               <tr>
                        <td>${item.apellidos}</td>
                        <td>${item.nombre}</td>
                        <td>${item.semestre}</td>
                        <td>${item.paralelo}</td>
                        <td>${item.direccion}</td>
                        <td>${item.telefono}</td>
                        <td>${item.correo}</td>                        
                </tr>               
               `
            }
        }
    }
}
